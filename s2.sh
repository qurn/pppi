#in gui
#add wifi
#change timezone
#update

#sudo pacman -S git base-devel
#mkdir build
#cd build
#git clone https://gitlab.com/qurn/pppi
#cd pppi
#bash s2.sh

#sudo pacman -S --needed --noconfirm go gvim linux-headers ripgrep fd wget android-tools
sudo pacman -Rc vim
sudo pacman -S --needed --noconfirm base-devel gvim

cd ~/build
git clone https://aur.archlinux.org/paru-bin.git
cd ~/build/paru-bin
makepkg -sri --noconfirm
paru -S --needed --noconfirm librewolf-bin nemo nemo-fileroller newsboat axolotl-bin

#remove concy
#fix keyboard
echo "export KEYBOARD_ARGS=\"-l full,special -H 200 -fn 'Hack 11'\"" >> ~/.config/sxmo/profile
