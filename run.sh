#!/bin/bash

# This script setup FDE on Arch Linux ARM for PinePhone
# and PineTab.
#
# Inspired by:
# https://github.com/sailfish-on-dontbeevil/flash-it

set +e

DOWNLOAD_SERVER="https://danctnix.arikawa-hi.me/rootfs/archarm-on-mobile"
TMPMOUNT=$(mktemp -p . -d)

# Helper functions
# Error out if the given command is not found in PATH.
function check_dependency {
    dependency=$1
    hash $dependency >/dev/null 2>&1 || {
        echo >&2 "${dependency} not found. Please make sure it is installed and on your PATH."; exit 1;
    }
}

function error {
    echo -e "\e[41m\e[5mERROR:\e[49m\e[25m $1"
}

pacman -S --needed --noconfirm parted cryptsetup wget tar f2fs-tools curl squashfs-tools arch-install-scripts

# Check dependencies
check_dependency "parted"
check_dependency "cryptsetup"
check_dependency "sudo"
check_dependency "wget"
check_dependency "tar"
check_dependency "unsquashfs"
check_dependency "mkfs.ext4"
check_dependency "mkfs.f2fs"
check_dependency "losetup"
check_dependency "zstd"
check_dependency "curl"

DEVICE="pinephone-pro"
USR_ENV="sxmo"

#SQFSDATE=$(curl -s -f $DOWNLOAD_SERVER/version.txt || echo BAD)
#SQFSDATE="20220501"
SQFSDATE=$(curl -s -f $DOWNLOAD_SERVER/version.txt || echo BAD)
SQFSROOT="archlinux-$DEVICE-$USR_ENV-$SQFSDATE.sqfs"
#SQFSROOT="../arch-pine64-build/build/archlinux-$DEVICE-$USR_ENV-$SQFSDATE.sqfs"

[ $SQFSDATE = "BAD" ] && { error "Failed to fetch for the latest version. The server may be down, please try again later." && exit 1; }




if [[ -f "$SQFSROOT" ]]
then
    echo "using old arch sqfsroot. If in doubt delete next time before running"
else
    wget --quiet --show-progress -c -O $SQFSROOT $DOWNLOAD_SERVER/$SQFSROOT || {
        error "Root filesystem image download failed. Aborting."
    	exit 2
}
fi


# Filesystem select
FILESYSTEM="ext4"
#echo -e "\e[1mWhich filesystem would you like to use?\e[0m"
#select OPTION in "ext4" "f2fs"; do
#    case $OPTION in
#        "ext4" ) FILESYSTEM="ext4"; break;;
#        "f2fs" ) FILESYSTEM="f2fs"; break;;
#    esac
#done

# Select flash target
lsblk
echo -e "\e[1mFlash where?\e[0m"
select OPTION in "emmc" "ssd"; do
    case $OPTION in
        "emmc" ) DISK_IMAGE="/dev/mmcblk2"; break;;
        "ssd" ) DISK_IMAGE="/dev/mmcblk1"; break;;
    esac
done


#echo -e "\e[1mWhich SD card do you want to flash?\e[0m"
#lsblk
#read -p "Device node (/dev/sdX): " DISK_IMAGE
#echo "Flashing image to: $DISK_IMAGE"
#echo "WARNING: All data will be erased! You have been warned!"
#echo "Some commands require root permissions, you might be asked to enter your sudo password."

## Make sure people won't pick the wrong thing and ultimately erase the disk
#echo
#echo -e "\e[31m\e[1mARE YOU SURE \e[5m\e[4m${DISK_IMAGE}\e[24m\e[25m IS WHAT YOU PICKED?\e[39m\e[0m"
#read -p "Confirm device node: " CONFIRM_DISK_IMAGE
#[ "$DISK_IMAGE" != "$CONFIRM_DISK_IMAGE" ] && error "The device node mismatched. Aborting." && exit 1
#echo

# Downloading images
#echo -e "\e[1mDownloading images...\e[0m"

#wget --quiet --show-progress -c -O $SQFSROOT $DOWNLOAD_SERVER/$SQFSROOT || {
#    error "Root filesystem image download failed. Aborting."
#    exit 2
#}

# Checksum check, make sure the root image is the real deal.
#curl -s $DOWNLOAD_SERVER/$SQFSROOT.sha512sum | sha512sum -c || { error "Checksum does not match. Aborting." && rm $SQFSROOT && exit 1; }

AIS="arch-install-scripts.tar.zst"
if [[ -f "$AIS" ]]
then
    echo "using old arch install script. If in doubt delete next time before running"
else
    wget --quiet --show-progress -c -O arch-install-scripts.tar.zst "https://archlinux.org/packages/extra/any/arch-install-scripts/download/" || {
    	error "arch-install-scripts download failed. Aborting."
    	exit 2
}
fi

tar --transform='s,^\([^/][^/]*/\)\+,,' -xf arch-install-scripts.tar.zst usr/bin/genfstab
chmod +x genfstab

[ ! -e "genfstab" ] && error "Failed to locate genfstab. Aborting." && exit 2

[ $FILESYSTEM = "ext4" ] && MKFS="mkfs.ext4"

sudo parted -a optimal ${DISK_IMAGE} mklabel msdos --script
sudo parted -a optimal ${DISK_IMAGE} mkpart primary fat32 65536s 589823s --script
sudo parted -a optimal ${DISK_IMAGE} mkpart primary ext4 589824s 100% --script
sudo parted ${DISK_IMAGE} set 1 boot on --script

# The first partition is the boot partition and the second one the root
PARTITIONS=$(lsblk $DISK_IMAGE -l | grep ' part ' | awk '{print $1}')
BOOTPART=/dev/$(echo "$PARTITIONS" | sed -n '1p')
ROOTPART=/dev/$(echo "$PARTITIONS" | sed -n '2p')

ENCRYNAME=$(basename $(mktemp -p /dev/mapper/ -u))
ENCRYPART="/dev/mapper/$ENCRYNAME"

echo "You'll now be asked to type in a new encryption key."

# Generating LUKS header on a modern computer would make the container slow to unlock
# on slower devices such as PinePhone.
#
# Unless you're happy with the phone taking an eternity to unlock, this is it for now.
sudo cryptsetup -q -y -v luksFormat --pbkdf-memory=20721 --pbkdf-parallel=4 --pbkdf-force-iterations=4 $ROOTPART
sudo cryptsetup open $ROOTPART $ENCRYNAME

[ ! -e /dev/mapper/${ENCRYNAME} ] && error "Failed to locate rootfs mapper. Aborting." && exit 1

sudo mkfs.vfat $BOOTPART
sudo $MKFS $ENCRYPART

sudo mount $ENCRYPART $TMPMOUNT
sudo mkdir $TMPMOUNT/boot
sudo mount $BOOTPART $TMPMOUNT/boot

sudo unsquashfs -f -d $TMPMOUNT $SQFSROOT

./genfstab -U $TMPMOUNT | grep UUID | grep -v "swap" | sudo tee -a $TMPMOUNT/etc/fstab
sudo sed -i "s:UUID=[0-9a-f-]*\s*/\s:/dev/mapper/cryptroot / :g" $TMPMOUNT/etc/fstab

pacstrap -GM $TMPMOUNT --needed --noconfirm go linux-headers ripgrep fd wget android-tools

#mkdir $TMPMOUNT/etc/myarch
#cp inside_chroot.sh $TMPMOUNT/etc/myarch/

#chmod +x $TMPMOUNT/etc/myarch/inside_chroot.sh
#arch-chroot $TMPMOUNT /bin/bash -c "su - -c /etc/myarch/inside_chroot.sh"

#sudo dd if=${TMPMOUNT}/boot/idbloader.img of=${DISK_IMAGE} seek=64 conv=notrunc,fsync
#sudo dd if=${TMPMOUNT}/boot/u-boot.itb of=${DISK_IMAGE} seek=16384 conv=notrunc,fsync

sudo umount -R $TMPMOUNT
sudo cryptsetup close $ENCRYNAME

#echo -e "\e[1mCleaning up working directory...\e[0m"
#sudo rm -f arch-install-scripts.tar.zst || true
#sudo rm -f genfstab || true
#sudo rm -rf $TMPMOUNT || true

#echo -e "\e[32m\e[1mAll done! Please insert the card to your device and power on.\e[39m\e[0m"
